#!/usr/bin/python3
import psycopg2
from create import *
from insert import *
from contraintes import *
from tkinter import *

# Interface de connexion à la base
window = Tk()
window.title("PROJET 2")
window.minsize = (480, 600)

heading_label = Label(window, text = "CONNEXION À LA BASE", activeforeground = 'blue')
heading_label.grid(row = 0, columnspan = 2, sticky = W + E, padx = 10, pady = 10)

E_host = Entry(window)
E_host.grid(row = 1, column = 1, sticky = E, padx = 10, pady = 10)
L_host = Label(window, text = "HOST : ")
L_host.grid(row = 1, column = 0, sticky = E, padx = 10, pady = 10)

E_user = Entry(window)
E_user.grid(row = 2, column = 1, sticky = E, padx = 10, pady = 10)
L_user = Label(window, text = "USER : ")
L_user.grid(row = 2, column = 0, sticky = E, padx = 10, pady = 10)

E_password = Entry(window)
E_password.grid(row = 3, column = 1, sticky = E, padx = 10, pady = 10)
L_password = Label(window, text = "PASSWORD : ")
L_password.grid(row = 3, column = 0, sticky = E, padx = 10, pady = 10)

E_database = Entry(window)
E_database.grid(row = 4, column = 1, sticky = E, padx = 10, pady = 10)
L_database = Label(window, text = "DATABASE : ")
L_database.grid(row = 4, column = 0, sticky = E, padx = 10, pady = 10)

def connexion(H, DB, U, P) :
    try :
        global conn
        conn = psycopg2.connect("host = '{}' dbname = '{}' user = '{}' password = '{}'".format(H, DB, U, P))
    except :
        print("La connexion a échoué.")
    window.destroy()

button = Button(window, text = "CONNEXION", command = lambda : connexion(H = E_host.get(), DB = E_database.get(), U = E_user.get(), P = E_password.get()))
button.grid(row = 5, columnspan = 2, sticky = W + E, padx = 30, pady = 10)

window.mainloop()

# Exécution des fichiers create.sql, insert.sql et contraintes.sql
cur = conn.cursor()
cur.execute(create_code)
cur.execute(insert_code)
cur.execute(contraintes_code)

# MENU
choice = '1'
while choice == '1' or choice == '2' or choice == '3' :
    print("\nQue souhaitez vous faire ?")
    print ("1. VOIR LES TABLES DE LA BASE DE DONNÉES")
    print ("2. VÉRIFIER QUE LES CONTRAINTES SOIENT RESPECTÉES")
    print ("3. INSÉRER DES VALEURS DANS LA BASE DE DONNÉES")
    print ("4. SORTIE")
    choice = input()
    if choice == '1' :
        choice_2 = '1'
        while choice_2 in ['1', '2', '3', '4', '5', '6'] :
            print("\nQuelle table souhaitez vous consulter ?")
            print ("1. UTILISATEURS")
            print ("2. GROUPES")
            print ("3. ENTREPRISES")
            print ("4. FLUX")
            print ("5. PUBLICATIONS")
            print ("6. NOTES D'UN RESPONSABLE")
            print ("7. SORTIE")
            choice_2 = input()

            if choice_2 == '1' :
                cur.execute("SELECT prenom, nom, email from utilisateur")
                res = cur.fetchall()
                print("\nUTILISATEURS :")
                i = 1
                for raw in res:
                  print ("{}. Prénom : {} || Nom : {} || Email : {}".format(i, raw[0], raw[1], raw[2]))
                  i += 1
            elif choice_2 == '2' :
                cur.execute("SELECT nom, type, responsable from groupe")
                res = cur.fetchall()
                print("\nGROUPES :")
                i = 1
                for raw in res:
                  print ("{}. Nom : {} || Type : {} || Responsable : {}".format(i, raw[0], raw[1], raw[2]))
                  i += 1
            elif choice_2 == '3' :
                cur.execute("SELECT siret, nom, codePostal from entreprise")
                res = cur.fetchall()
                print("\nENTREPRISES :")
                i = 1
                for raw in res:
                  print ("{}. Siret : {} || Nom : {} || CodePostal : {}".format(i, raw[0], raw[1], raw[2]))
                  i += 1
            elif choice_2 == '4' :
                cur.execute("SELECT nom, description, responsable from flux")
                res = cur.fetchall()
                print("\nFLUX :")
                i = 1
                for raw in res:
                  print ("{}. Nom : {} || Description : {} || Responsable : {}".format(i, raw[0], raw[1], raw[2]))
                  i += 1
            elif choice_2 == '5' :
                cur.execute("SELECT ps.titre, ps.description, ps.flux FROM publicationSeul ps UNION SELECT pp.titre, pp.description, pp.flux FROM publicationPlusieurs pp")
                res = cur.fetchall()
                print("\nPUBLICATIONS :")
                i = 1
                for raw in res:
                  print ("{}. Titre : {} || Description : {} || Flux : {}".format(i, raw[0], raw[1], raw[2]))
                  i += 1
            elif choice_2 == '6' :
                cur.execute("SELECT titre, description, flux from noteResponsable")
                res = cur.fetchall()
                print("\nNOTES D'UN RESPONSABLE :")
                i = 1
                for raw in res:
                  print ("{}. Titre : {} || Description : {} || Flux : {}".format(i, raw[0], raw[1], raw[2]))
                  i += 1

    if choice == '2' :
        choice_2 = '1'
        while choice_2 in ['1', '2', '3', '4', '5', '6', '7'] :
          print ("\nMENU")
          print ("1. AU MOINS 1 GROUPE LECTEUR PAR FLUX")
          print ("2. AU MOINS 1 GROUPE RÉDACTEUR PAR FLUX")
          print ("3. AU MOINS 2 UTILISATEURS PAR RÉUNION")
          print ("4. AU MOINS 2 UTILISATEURS PAR PUBLICATIONSEUL")
          print ("5. TOUS LES GROUPES D'UN FLUX SONT DE LA MÊME ENTREPRISE")
          print ("6. TOUS LES GROUPES D'UN UTILISATEUR SONT DE LA MÊME ENTREPRISE")
          print ("7. POUR CHAQUE PUBLICATION SEUL, UTILISATEUR APPARTIENT À AU MOINS UN GROUPE RÉDACTEUR DU FLUX")
          print ("8. SORTIE")
          choice_2 = input()

          if choice_2 == '1' :
              cur.execute("SELECT * from v_lecteur_flux")
              res = cur.fetchall()
              print("\nFLUX N'AYANT PAS DE GROUPES LECTEUR :")
              i = 1
              for raw in res:
                print ("{}. {}".format(i, raw[0]))
                i += 1
          elif choice_2 == '2' :
              cur.execute("SELECT * from v_redacteur_flux")
              res = cur.fetchall()
              print("\nFLUX N'AYANT PAS DE GROUPES REDACTEUR :")
              i = 1
              for raw in res:
                 print ("{}. {}".format(i, raw[0]))
                 i += 1
          elif choice_2 == '3' :
              cur.execute("SELECT reunion, nombre_utilisateurs FROM v_nombre_utilisateurs_reunion")
              res = cur.fetchall()
              print("\nNOMBRE DE PERSONNES PAR RÉUNION :")
              i = 1
              for raw in res:
                print ("{}. Reunion : {} || Nombre d'utiliateurs : {}".format(i, raw[0], raw[1]))
                i += 1
          elif choice_2 == '4' :
              cur.execute("SELECT publicationPlusieurs, nombre_utilisateurs FROM v_nombre_utilisateurs_publicationPlusieurs")
              res = cur.fetchall()
              print("\nNOMBRE D'UTILISATEURS PAR PUBLICATIONSEUL :")
              i = 1
              for raw in res:
                print ("{}. PublicationSeul : {} || Nombre d'utiliateurs : {}".format(i, raw[0], raw[1]))
                i += 1
          elif choice_2 == '5' :
              cur.execute("SELECT flux FROM v_flux_groupe_entreprise")
              res = cur.fetchall()
              print("\nFLUX POSSÉDANT DES GROUPES D'ENTREPRISES DIFFÉRENTES :")
              i = 1
              for raw in res:
                print ("{}. {}".format(i, raw[0]))
                i += 1
          elif choice_2 == '6' :
              cur.execute("SELECT email_utilisateur FROM v_nombre_entreprises_groupes_utilisateur WHERE nombre_entreprises > 1")
              res = cur.fetchall()
              print("\nUTILISATEURS APPARTENANT À DES GROUPES D'ENTREPRISES DIFFÉRENTES :")
              i = 1
              for raw in res:
                print ("{}. {}".format(i, raw[0]))
                i += 1
          elif choice_2 == '7' :
              cur.execute("SELECT ps.titre, ps.utilisateur FROM v_nombre_groupes_redacteurs_flux_utilisateur vngrfu JOIN publicationSeul ps ON ps.id = vngrfu.publication WHERE nombre_groupes_redacteurs_flux_utilisateur < 1")
              res = cur.fetchall()
              print("\nPUBLICATIONS DONT L'UTILISATEUR N'APPARTIENT PAS À UN GROUPE RÉDACTEUR DU FLUX :")
              i = 1
              for raw in res:
                print ("{}. {} par {}".format(i, raw[0], raw[1]))
                i += 1

    if choice == '3' :
        choice_2 = '1'
        while choice_2 in ['1', '2', '3', '4', '5', '6'] :
            print("\nDans quelle table souhaitez vous insérer des valeurs ?")
            print ("1. UTILISATEUR")
            print ("2. GROUPE")
            print ("3. ENTREPRISE")
            print ("4. FLUX")
            print ("5. PUBLICATIONSEUL")
            print ("6. NOTERESPONSABLE")
            print ("7. SORTIE")
            choice_2 = input()

            if choice_2 == '1' :
                print("email :")
                email = input()
                print("nom :")
                nom = input()
                print("prénom :")
                prenom = input()
                print("date de naissance :")
                dateNaissance = input()
                print("genre :")
                genre = input()
                print("pays :")
                pays = input()
                print("métier :")
                metier = input()
                print("entreprise :")
                entreprise = input()
                try :
                    cur.execute("INSERT INTO utilisateur VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', {})".format(email, nom, prenom, dateNaissance, genre, pays, metier, entreprise))
                    conn.commit()
                except psycopg2.IntegrityError as e :
                    print("Message système :", e)
                except psycopg2.DataError as e :
                    print("Message système :", e)
                except :
                    print("Échec insertion")

            if choice_2 == '2' :
                print("nom :")
                nom = input()
                print("type :")
                type = input()
                print("responsable :")
                responsable = input()
                try :
                    cur.execute("INSERT INTO groupe VALUES ('{}', '{}', '{}')".format(nom, type, responsable))
                    conn.commit()
                except psycopg2.IntegrityError as e :
                    print("Message système :", e)
                except psycopg2.DataError as e :
                    print("Message système :", e)
                except :
                    print("Échec insertion")

            if choice_2 == '3' :
                print("siret :")
                siret = input()
                print("nom :")
                nom = input()
                print("codePostal :")
                codePostal = input()
                print("dateCreation :")
                dateCreation = input()
                try :
                    cur.execute("INSERT INTO entreprise VALUES ({}, '{}', {}, '{}')".format(siret, nom, codePostal, dateCreation))
                    conn.commit()
                except psycopg2.IntegrityError as e :
                    print("Message système :", e)
                except psycopg2.DataError as e :
                    print("Message système :", e)
                except :
                    print("Échec insertion")

            if choice_2 == '4' :
                print("nom :")
                nom = input()
                print("description :")
                description = input()
                print("date de création :")
                dateCreation = input()
                print("responsable :")
                responsable = input()
                try :
                    cur.execute("INSERT INTO flux VALUES ('{}', '{}', '{}', '{}')".format(nom, description, dateCreation, responsable))
                    conn.commit()
                except psycopg2.IntegrityError as e :
                    print("Message système :", e)
                except psycopg2.DataError as e :
                    print("Message système :", e)
                except :
                    print("Échec insertion")

            if choice_2 == '5' :
                print("id :")
                id = input()
                print("titre :")
                titre = input()
                print("description :")
                description = input()
                print("liensImages :")
                liensImages = input()
                print("datePublication :")
                datePublication = input()
                print("flux :")
                flux = input()
                print("responsable :")
                responsable = input()
                try :
                    cur.execute("INSERT INTO publicationSeul VALUES ({}, '{}', '{}', '{}', '{}', '{}', '{}')".format(id, titre, description, liensImages, datePublication, flux, utilisateur))
                    conn.commit()
                except psycopg2.IntegrityError as e :
                    print("Message système :", e)
                except psycopg2.DataError as e :
                    print("Message système :", e)
                except :
                    print("Échec insertion")

            if choice_2 == '6' :
                print("id :")
                id = input()
                print("titre :")
                titre = input()
                print("description :")
                description = input()
                print("liensImages :")
                liensImages = input()
                print("dateNoteResponsable :")
                dateNoteResponsable = input()
                print("flux :")
                flux = input()
                print("responsable :")
                responsable = input()
                try :
                    cur.execute("INSERT INTO noteResponsable VALUES ({}, '{}', '{}', '{}', '{}', '{}', '{}')".format(id, titre, description, liensImages, datePublication, flux, utilisateur))
                    conn.commit()
                except psycopg2.IntegrityError as e :
                    print("Message système :", e)
                except psycopg2.DataError as e :
                    print("Message système :", e)
                except :
                    print("Échec insertion")
