contraintes_code = """
DROP VIEW IF EXISTS v_nombre_groupes_redacteurs_flux_utilisateur ;
DROP VIEW IF EXISTS R5 ;
DROP VIEW IF EXISTS R4 ;
DROP VIEW IF EXISTS R3 ;
DROP VIEW IF EXISTS v_utilisateur ;
DROP VIEW IF EXISTS v_groupe ;
DROP VIEW IF EXISTS v_responsableflux ;
DROP VIEW IF EXISTS v_lecteur_flux ;
DROP VIEW IF EXISTS R1 ;
DROP VIEW IF EXISTS v_redacteur_flux ;
DROP VIEW IF EXISTS R2 ;
DROP VIEW IF EXISTS v_nombre_utilisateurs_reunion ;
DROP VIEW IF EXISTS v_nombre_utilisateurs_publicationPlusieurs ;
DROP VIEW IF EXISTS v_nombre_entreprises_groupes_utilisateur ;
DROP VIEW IF EXISTS v_flux_groupe_entreprise ;
DROP VIEW IF EXISTS v_entreprise_flux ;
DROP VIEW IF EXISTS v_entreprise_groupe ;
DROP VIEW IF EXISTS v_discussion_1_seul_utilisateur ;

    -- vues permettant de detecter des anomalies(contraintes compliquees) --

-- projection(responsableFlux(email)) = projection(flux(responsable))
CREATE VIEW v_responsableflux(resp_email, flux_resp)
AS
SELECT responsableflux.email, flux.responsable
FROM responsableflux LEFT JOIN flux
ON responsableflux.email = flux.responsable
WHERE flux.responsable = NULL
;

-- projection(groupe(nom)) = projection(groupe_utilisateur(groupe))
CREATE VIEW v_groupe(nom_groupe, nom_groupe_utilisateur)
AS
SELECT groupe.nom, groupe_utilisateur.groupe
FROM groupe LEFT JOIN groupe_utilisateur
ON groupe.nom = groupe_utilisateur.groupe
WHERE groupe_utilisateur.groupe = NULL
;

-- projection(utilisateur(email)) = projection(groupe_utilisateur(utilisateur))
CREATE VIEW v_utilisateur(email_utilisateur, utilisateur_groupe_utilisateur)
AS
SELECT utilisateur.email, groupe_utilisateur.utilisateur
FROM utilisateur LEFT JOIN groupe_utilisateur
ON utilisateur.email = groupe_utilisateur.utilisateur
WHERE groupe_utilisateur.utilisateur = NULL
;

-- R1 = restriction(groupe, type = "Lecteur")
CREATE VIEW R1(nom_groupe)
AS
SELECT nom
FROM groupe
WHERE type = 'lecteur'
;

-- projection(R1(nom)) = projection(groupe_flux(flux))
CREATE VIEW v_lecteur_flux(flux)
AS
SELECT groupe_flux.flux
FROM R1
RIGHT JOIN groupe_flux
ON R1.nom_groupe = groupe_flux.groupe
WHERE R1.nom_groupe IS NULL
;

-- R2 = restriction(groupe, type = "Rédacteur")
CREATE VIEW R2(nom_groupe)
AS
SELECT nom
FROM groupe
WHERE type = 'redacteur'
;

-- projection(R2(nom)) = projection(groupe_flux(flux))
CREATE VIEW v_redacteur_flux(flux)
AS
SELECT groupe_flux.flux
FROM R2
RIGHT JOIN groupe_flux
ON R2.nom_groupe = groupe_flux.groupe
WHERE R2.nom_groupe IS NULL
;

-- au moins 2 utilisateurs par réunion
CREATE VIEW v_nombre_utilisateurs_reunion(reunion, nombre_utilisateurs)
AS
SELECT reunion.id, COUNT(reunion_utilisateur.utilisateur)
FROM reunion LEFT JOIN reunion_utilisateur
ON reunion.id = reunion_utilisateur.reunion
GROUP BY reunion.id
;

-- au moins 2 utilisateurs par publicationPlusieurs
CREATE VIEW v_nombre_utilisateurs_publicationPlusieurs(publicationPlusieurs, nombre_utilisateurs)
AS
SELECT publicationPlusieurs.id, COUNT(publicationPlusieurs_utilisateur.utilisateur)
FROM publicationPlusieurs LEFT JOIN publicationPlusieurs_utilisateur
ON publicationPlusieurs.id = publicationPlusieurs_utilisateur.publicationPlusieurs
GROUP BY publicationPlusieurs.id
;

-- table discussion, utilisateur1 ≠ utilisateur2
CREATE VIEW v_discussion_1_seul_utilisateur(utilisateur1, utilisateur2)
AS
SELECT *
FROM discussion
WHERE utilisateur1 = utilisateur2
;

-- entreprise de chaque flux
CREATE VIEW v_entreprise_flux(flux, entreprise)
AS
SELECT f.nom, e.nom
FROM flux f
JOIN responsableFlux r
ON f.responsable = r.email
JOIN utilisateur u
ON r.email = u.email
JOIN entreprise e
ON u.entreprise = e.siret
;

-- entreprise de chaque groupe
CREATE VIEW v_entreprise_groupe(groupe, entreprise)
AS
SELECT g.nom, e.nom
FROM groupe g
JOIN utilisateur u
ON g.responsable = u.email
JOIN entreprise e
ON u.entreprise = e.siret
;

-- les flux ne doivent "posséder" que des groupes de la même entreprise
CREATE VIEW v_flux_groupe_entreprise(flux, groupe, entreprise_flux, entreprise_groupe)
AS
SELECT gf.flux, gf.groupe, vef.entreprise, veg.entreprise
FROM groupe_flux gf
JOIN v_entreprise_flux vef
ON gf.flux = vef.flux
JOIN v_entreprise_groupe veg
ON gf.groupe = veg.groupe
WHERE vef.entreprise != veg.entreprise
;

-- tous les groupes auxquels appartient un utilisateur doivent être des groupes de la même entreprise
CREATE VIEW v_nombre_entreprises_groupes_utilisateur(email_utilisateur, nombre_entreprises)
AS
SELECT gu.utilisateur, COUNT(DISTINCT veg.entreprise)
FROM groupe_utilisateur gu JOIN v_entreprise_groupe veg
ON gu.groupe = veg.groupe
GROUP BY gu.utilisateur
;

-- projection(jointure(publication, groupe_utilisateur, p.utilisateur = gu.utilisateur), groupe)
CREATE VIEW R3(publication, flux, utilisateur, groupe)
AS
SELECT p.id, p.flux, p.utilisateur, gu.groupe
FROM publicationSeul p JOIN groupe_utilisateur gu
ON p.utilisateur = gu.utilisateur
;

CREATE VIEW R4(groupe, flux)
AS
SELECT R2.nom_groupe, groupe_flux.flux
FROM R2 JOIN groupe_flux
ON R2.nom_groupe = groupe_flux.groupe
;

CREATE VIEW R5(publication, flux, utilisateur, groupes_utilisateur, groupe_flux)
AS
SELECT R3.*, R4.groupe,
CASE
  WHEN R3.groupe = R4.groupe THEN 1
  ELSE 0
 END as test
FROM R3 JOIN R4
ON R3.flux = R4.flux
;

-- pour chaque publicationSeul, utilisateur appartient à au moins un groupe rédacteur du flux
CREATE VIEW v_nombre_groupes_redacteurs_flux_utilisateur(publication, utilisateur, flux, nombre_groupes_redacteurs_flux_utilisateur)
AS
SELECT publication, utilisateur, flux, SUM(test)
FROM R5
GROUP BY publication, utilisateur, flux
ORDER BY publication
;

"""
