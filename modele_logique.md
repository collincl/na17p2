## MODELE LOGIQUE

`entreprise` (#SIRET : NUMERIC(14), nom : VARCHAR(25), codePostal : NUMERIC(5), ville : VARCHAR(25), region : VARCHAR(25), dateCreation : DATE) <br>
> _contraintes :_ <br>
\+ not null(nom), not null(codePostal), not null(ville), not null(region), not null(dateCreation) <br>
\+ dateCreation < current_date()

 `utilisateur` (#email : VARCHAR(40), nom : VARCHAR(25), prenom : VARCHAR(25), dateNaissance : DATE, genre : {homme|femme}, pays : VARCHAR(25), metier : VARCHAR(25), entreprise : NUMERIC(14) ➔ entreprise) <br>
> _contraintes :_ <br>
\+ not null(nom), not null(prenom), not null(dateNaissance), not null(pays), not null(metier) <br>
\+ not null(entreprise) <br>
\+ dateNaissance < current_date()

`responsableFlux` (#email : VARCHAR(40) ➔ utilisateur) <br>
> _contrainte :_ <br>
\+ projection(responsableFlux(email)) = projection(flux(responsable))

`groupe` (#nom : VARCHAR(25), type : {lecteur|redacteur}, responsable : VARCHAR(40) ➔ utilisateur, entreprise : NUMERIC(14) ➔ entreprise) <br>
> _contraintes :_ <br>
\+ not null(type) <br>
\+ not null(responsable) <br>
\+ not null(entreprise)

`flux` (#nom : VARCHAR(25), description : VARCHAR(300), dateCreation : DATE, responsable : VARCHAR(40) ➔ responsableFlux, entreprise : NUMERIC(14) ➔ entreprise) <br>
> _contraintes :_ <br>
\+ not null(dateCreation) <br>
\+ not null(responsable) <br>
\+ not null(entreprise)

`publicationSeul` (#id : INTEGER, titre : VARCHAR(25), description : VARCHAR(300), lienImage : VARCHAR(300), date : DATE, flux : VARCHAR(25) ➔ flux, utilisateur : VARCHAR(40) ➔ utilisateur) <br>
> _contraintes :_ <br>
\+ not null(titre) <br>
\+ not null(flux) <br>
\+ not null(utilisateur) <br>
\+ utilisateur appartient à un groupe rédacteur du flux

`publicationPlusieurs` (#id : INTEGER, titre : VARCHAR(25), description : VARCHAR(300), lienImage : VARCHAR(300), date : DATE, flux : VARCHAR(25) ➔ flux) <br>
> _contraintes :_ <br>
\+ not null(titre)<br>
\+ not null(flux) <br>
\+ not null(utilisateur)

`noteResponsable` (#id : INTEGER, titre : VARCHAR(25), description : VARCHAR(300), lienImage : VARCHAR(300), date : DATE, flux : VARCHAR(25) ➔ flux, responsable : VARCHAR(40) ➔ responsableFlux) <br>
> _contraintes :_ <br>
\+ not null(titre) <br>
\+ not null(flux) <br>
\+ not null(responsable)

`reunion` (#id : INTEGER, dateReunion : DATE, lieuReunion : VARCHAR(200), titre : VARCHAR(25), duree : INTEGER)
> _contrainte :_ <br>
\+ not null(titre) <br>
\+ dateCreation < current_date()

`vote` (#publication : INTEGER ➔ publicationSeul, #utilisateur : VARCHAR(40) ➔ utilisateur, contenu : {+1|-1}, date : DATE) <br>
> _contrainte :_ <br>
\+ not null(contenu), not null(date) <br>
\+ not null(publication) <br>
\+ not null(utilisateur)

`commentaire` (#num : INTEGER, #publication : INTEGER ➔ publicationSeul, utilisateur : VARCHAR(40) ➔ utilisateur, contenu : VARCHAR(300), date : DATE) <br>
> _contrainte :_ <br>
\+ not null(contenu), not null(date) <br>
\+ not null(publication) <br>
\+ not null(utilisateur)

`discussion` (#utilisateur1 : VARCHAR(40) ➔ utilisateur, #utilisateur2 : VARCHAR(40) ➔ utilisateur) <br>
> _contrainte :_ <br>
\+ utilisateur1 ≠ utilisateur2

`message` (#num : INTEGER, contenu : VARCHAR(300), date : DATE, #discussion_utilisateur1 : VARCHAR(40) ➔ discussion, #discussion_utilisateur2 : VARCHAR(40) ➔ discussion) <br>
> _contrainte :_ <br>
\+ not null(contenu), not null(date) <br>
\+ not null(discussion)

`groupe_utilisateur` (#utilisateur : VARCHAR(40)  ➔ utilisateur, #groupe : VARCHAR(25) ➔ groupe) <br>
> _contraintes :_ <br>
\+ projection(groupe(nom)) = projection(groupe_utilisateur(nom)) <br>
\+ projection(utilisateur(groupe)) = projection(groupe_utilisateur(groupe)) <br>
\+ tous les groupes auxquels appartient un utilisateur doivent être des groupes de la même entreprise

`groupe_flux` (#flux : VARCHAR(25)  ➔ flux, #groupe : VARCHAR(25) ➔ groupe) <br>
> _contraintes :_ <br>
\+ R1 = restriction(groupe, type = "Lecteur") <br>
projection(R1(nom)) = projection(groupe_flux(flux)) <br>
\+ R2 = restriction(groupe, type = "Rédacteur") <br>
projection(R2(nom)) = projection(flux(flux)) <br>
\+ tous les groupes et les flux doivent être de la même entreprise

`publicationPlusieurs_utilisateur` (#publication : INTEGER ➔ publicationPlusieurs, #utilisateur : VARCHAR(40) ➔ utilisateur)
> _contrainte :_ <br>
\+ au moins 2 utilisateurs par publicationPlusieurs <br>
\+ utilisateurs appartiennent au groupe rédacteur du flux

`reunion_utilisateur` (#reunion : INTEGER ➔ reunion(id),  #utilisateur : VARCHAR(40) ➔ utilisateur)
> _contrainte :_ <br>
\+ au moins 2 utilisateurs par reunion
