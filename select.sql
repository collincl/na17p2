DROP VIEW IF EXISTS nombre_likes_publication ;
DROP VIEW IF EXISTS nombre_dislikes_publication ;

-- vue permettant de calculer le nombre de likes des publications
CREATE VIEW nombre_likes_publication(publication, nombre_likes)
AS
SELECT publicationSeul.id, COUNT(vote.publication)
FROM publicationSeul JOIN vote
ON publicationSeul.id = vote.publication
WHERE vote.vote = 'like'
GROUP BY publicationSeul.id
;

-- vue permettant de calculer le nomnre de dislikes des publications
CREATE VIEW nombre_dislikes_publication(publication, nombre_dislikes)
AS
SELECT publicationSeul.id, COUNT(vote.publication)
FROM publicationSeul JOIN vote
ON publicationSeul.id = vote.publication
WHERE vote.vote = 'dislike'
GROUP BY publicationSeul.id
;

-- affichages de toutes les publications
/*
SELECT ps.id, ps.titre, ps.description, CAST(l.* as TEXT), ps.flux, 'publicationSeul' as type FROM publicationSeul ps, JSON_ARRAY_ELEMENTS(ps.liensImages) l UNION
SELECT pp.id, pp.titre, pp.description, CAST(l.* as TEXT), pp.flux, 'publicationPlusieurs' as type FROM publicationPlusieurs pp, JSON_ARRAY_ELEMENTS(pp.liensImages) l
;
*/

-- âge des utilisateurs
/*
SELECT utilisateur.email, trunc((CURRENT_DATE - utilisateur.dateNaissance) / (365))
FROM utilisateur
;
*/

-- classement des publications du meilleur au pire score
/*
SELECT nombre_likes_publication.publication, (nombre_likes_publication.nombre_likes - nombre_dislikes_publication.nombre_dislikes) AS score
FROM nombre_dislikes_publication JOIN nombre_likes_publication
ON nombre_likes_publication.publication = nombre_dislikes_publication.publication
ORDER BY score DESC
;
*/

-- classement des publications seul de la plus récente à la plus ancienne
/*
SELECT *
FROM publicationSeul p
ORDER BY p.datePublication DESC
;
*/

-- classement des publications à plusieurs de la plus récente à la plus ancienne
/*
SELECT *
FROM publicationPlusieurs p
ORDER BY p.datePublication DESC
;
*/

-- utilisateurs qui sont responsables d'un groupe
/*
SELECT utilisateur.email, utilisateur.nom, utilisateur.prenom, groupe.nom
FROM utilisateur JOIN groupe
ON utilisateur.email = groupe.responsable
;
*/

-- adresse "complète" des réunions
/*
SELECT r.dateReunion, CAST(r.lieuReunion->>'numero' AS INTEGER) as numero, r.lieuReunion->>'voie' as voie, CAST(r.lieuReunion->>'codePostal' AS NUMERIC(5)) as cp, c.ville, c.region
FROM reunion r JOIN codePostal_ville_region c
ON CAST(r.lieuReunion->>'codePostal' AS NUMERIC(5)) = c.codePostal
;
*/

-- entreprise de chaque flux
/*
SELECT f.nom, e.nom
FROM flux f
JOIN responsableFlux r
ON f.responsable = r.email
JOIN utilisateur u
ON r.email = u.email
JOIN entreprise e
ON u.entreprise = e.siret
;
*/

-- entreprise de chaque groupe
/*
SELECT g.nom, e.nom
FROM groupe g
JOIN utilisateur u
ON g.responsable = u.email
JOIN entreprise e
ON u.entreprise = e.siret
;
*/

-- "dashboard" (tous les flux auxquels un utilisateur a accès)
/*
SELECT u.prenom, u.nom, gf.flux, g.type
FROM groupe_flux gf
JOIN groupe g
ON g.nom = gf.groupe
JOIN groupe_utilisateur gu
ON gu.groupe = gf.groupe
RIGHT JOIN utilisateur u
ON u.email = gu.utilisateur
;
*/

-- "profile" des utilisateurs
/*
SELECT u.prenom, u.nom, e.nom as entreprise, gu.groupe
FROM groupe g
JOIN groupe_utilisateur gu
ON gu.groupe = g.nom
RIGHT JOIN utilisateur u
ON u.email = gu.utilisateur
JOIN entreprise e
ON e.siret = u.entreprise
;
*/
