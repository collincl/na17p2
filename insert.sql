INSERT INTO codePostal_ville_region VALUES (75000, 'Paris', 'Ile-de-France') ;
INSERT INTO codePostal_ville_region VALUES (13000, 'Marseille', 'PACA') ;
INSERT INTO codePostal_ville_region VALUES (31000, 'Toulouse', 'Occitanie') ;
INSERT INTO codePostal_ville_region VALUES (33000, 'Bordeaux', 'Aquitaine') ;
INSERT INTO codePostal_ville_region VALUES (69000, 'Lyon', 'Rhone-Alpes') ;

INSERT INTO entreprise VALUES (36252187900034, 'Tektonik', 75000, '2017-12-23') ;
INSERT INTO entreprise VALUES (65436646556434, 'Skeleton', 33000, '1999-11-30') ;
INSERT INTO entreprise VALUES (36259875934556, 'Repair&fix', 13000, '2015-03-25') ;
INSERT INTO entreprise VALUES (19076378747635, 'Naturomax', 75000, '2007-05-09') ;
INSERT INTO entreprise VALUES (98765446322669, 'InfoParis', 31000, '2012-10-12') ;
INSERT INTO entreprise VALUES (87654567765456, 'Marketingzone', 69000, '1990-01-18') ;

INSERT INTO utilisateur VALUES ('clementcollin@gmail.com', 'Collin', 'Clément', '1999-12-18', 'homme', 'France', 'Réparateur', 36259875934556) ; -- 'Repair&fix'
INSERT INTO utilisateur VALUES ('martin.come123@gmail.com', 'Martin', 'Côme', '1990-10-05', 'homme', 'France', 'Electricien', 36259875934556) ; -- 'Repair&fix'
INSERT INTO utilisateur VALUES ('manu.dupre@yahoo.fr', 'Dupré', 'Emmanuel', '2001-01-03', 'homme', 'France', 'Plombier', 36259875934556) ; -- 'Repair&fix'
INSERT INTO utilisateur VALUES ('dance4ever@gmail.com', 'Dutronc', 'Liam', '1992-08-08', 'homme', 'France', 'Professeur de danse', 36252187900034) ; -- 'Tektonik'
INSERT INTO utilisateur VALUES ('leondanse@gmail.com', 'Legal', 'Léon', '1995-07-12', 'homme', 'France', 'Professeur de danse', 36252187900034) ; -- 'Tektonik'
INSERT INTO utilisateur VALUES ('laura982@gmail.com', 'Compte', 'Laura', '1999-04-03', 'femme', 'Espagne', 'Vendeuse', 19076378747635) ; -- 'Naturomax'
INSERT INTO utilisateur VALUES ('jeandujardin@yahoo.fr', 'Dujardin', 'Jean', '1970-12-10', 'homme', 'France', 'Commercial', 87654567765456) ; -- 'Marketingzone'
INSERT INTO utilisateur VALUES ('jennnnnn@gmail.com', 'Jenner', 'Kendall', '1998-10-10', 'femme', 'USA', 'Ingénieure', 87654567765456) ; -- 'Marketingzone'
INSERT INTO utilisateur VALUES ('123bella@gmail.com', 'Hadid', 'Bella', '1998-01-01', 'femme', 'USA', 'Graphiste', 87654567765456) ; -- 'Marketingzone'
INSERT INTO utilisateur VALUES ('jacksparrrow@gmail.com', 'Sparrow', 'Jack', '2000-06-28', 'homme', 'USA', 'Informaticien', 87654567765456) ; -- 'Marketingzone'

INSERT INTO responsableFlux VALUES ('clementcollin@gmail.com') ; -- 'Repair&fix'
INSERT INTO responsableFlux VALUES ('jennnnnn@gmail.com') ; -- 'Marketingzone'
INSERT INTO responsableFlux VALUES ('leondanse@gmail.com') ; -- 'Tektonik'

INSERT INTO groupe VALUES ('Repair&fix direction', 'redacteur', 'clementcollin@gmail.com') ; -- 'Repair&fix'
INSERT INTO groupe VALUES ('Tektonik Managers', 'redacteur', 'dance4ever@gmail.com') ; -- 'Tektonik'
INSERT INTO groupe VALUES ('Tektonik Collaborateurs', 'lecteur', 'dance4ever@gmail.com') ; -- 'Tektonik'
INSERT INTO groupe VALUES ('Naturomax Rue Léon Cladel', 'lecteur', 'laura982@gmail.com') ; -- 'Naturomax'
INSERT INTO groupe VALUES ('Marketingzone 3rd floor', 'redacteur', 'jacksparrrow@gmail.com') ; -- 'Marketingzone'
INSERT INTO groupe VALUES ('Marketingzone bis', 'lecteur', 'jacksparrrow@gmail.com') ; -- 'Marketingzone'

INSERT INTO groupe_utilisateur VALUES ('jennnnnn@gmail.com', 'Marketingzone 3rd floor') ; -- 'Marketingzone' - REDACTEUR
INSERT INTO groupe_utilisateur VALUES ('jacksparrrow@gmail.com', 'Marketingzone 3rd floor') ; -- 'Marketingzone' - REDACTEUR
INSERT INTO groupe_utilisateur VALUES ('jeandujardin@yahoo.fr', 'Marketingzone bis') ; -- 'Marketingzone' - LECTEUR
INSERT INTO groupe_utilisateur VALUES ('123bella@gmail.com', 'Marketingzone bis') ; -- 'Marketingzone' - LECTEUR
INSERT INTO groupe_utilisateur VALUES ('dance4ever@gmail.com', 'Tektonik Collaborateurs') ; -- 'Naturomax' - LECTEUR
INSERT INTO groupe_utilisateur VALUES ('dance4ever@gmail.com', 'Tektonik Managers') ; -- 'Tektonik' - REDACTEUR
INSERT INTO groupe_utilisateur VALUES ('leondanse@gmail.com', 'Tektonik Managers') ; -- 'Tektonik' - REDACTEUR
INSERT INTO groupe_utilisateur VALUES ('leondanse@gmail.com', 'Tektonik Collaborateurs') ; -- 'Tektonik' - REDACTEUR
INSERT INTO groupe_utilisateur VALUES ('martin.come123@gmail.com', 'Repair&fix direction') ; -- 'Repair&fix' - REDACTEUR
INSERT INTO groupe_utilisateur VALUES ('clementcollin@gmail.com', 'Repair&fix direction') ; -- 'Repair&fix' - REDACTEUR
INSERT INTO groupe_utilisateur VALUES ('clementcollin@gmail.com', 'Tektonik Managers') ; -- TEST - REDACTEUR


INSERT INTO flux VALUES ('Vacances', 'Photos de vacances', '2020-01-02', 'clementcollin@gmail.com') ; -- 'Repair&fix'
INSERT INTO flux VALUES ('News', 'Forum des nouvelles', '2018-08-08', 'leondanse@gmail.com') ; -- 'Tektonik'
INSERT INTO flux VALUES ('Superbowl', 'Projet publicité Superbowl', '2019-11-05', 'jennnnnn@gmail.com') ; -- 'Marketingzone'
INSERT INTO flux VALUES ('test', 'test', '2019-11-05', 'jennnnnn@gmail.com') ; -- 'Marketingzone'


INSERT INTO groupe_flux VALUES ('Marketingzone 3rd floor', 'Superbowl') ; -- 'Marketingzone'
INSERT INTO groupe_flux VALUES ('Tektonik Collaborateurs', 'News') ; -- 'Tektonik'
INSERT INTO groupe_flux VALUES ('Tektonik Managers', 'News') ; -- 'Tektonik'
INSERT INTO groupe_flux VALUES ('Repair&fix direction', 'Vacances') ; -- 'Repair&fix'
INSERT INTO groupe_flux VALUES ('Tektonik Managers', 'Vacances') ; -- 'TEST'

INSERT INTO publicationSeul(id, titre, description, datePublication, flux, utilisateur) VALUES (1, 'Update n°1 Projet', 'Mise à jour projet Superbowl', '2019-11-06', 'Superbowl', 'jennnnnn@gmail.com') ; -- 'Marketingzone'
INSERT INTO publicationSeul(id, titre, description, datePublication, flux, utilisateur) VALUES (2, 'Update n°2 Projet', 'Mise à jour projet Superbowl', '2019-11-07', 'Superbowl', 'jennnnnn@gmail.com') ; -- 'Marketingzone'
INSERT INTO publicationSeul(id, titre, description, datePublication, flux, utilisateur) VALUES (3, 'Update n°3 Projet', 'Mise à jour projet Superbowl', '2019-11-10', 'Superbowl', 'jacksparrrow@gmail.com') ; -- 'Marketingzone'
INSERT INTO publicationSeul(id, titre, description, datePublication, flux, utilisateur) VALUES (4, 'News 15/08/18', 'News 15/08/18', '2018-08-15', 'News', 'leondanse@gmail.com') ; -- 'Tektonik'
INSERT INTO publicationSeul(id, titre, description, datePublication, flux, utilisateur) VALUES (5, 'News 16/08/18', 'News 16/08/18', '2018-08-16', 'News', 'leondanse@gmail.com') ; -- 'Tektonik'
INSERT INTO publicationSeul(id, titre, liensImages, datePublication, flux, utilisateur) VALUES (6, 'Photos Maroc', '["lienImage1", "lienImage2", "lienImage3"]','2018-02-14', 'Vacances', 'martin.come123@gmail.com') ; -- 'Repair&fix'
INSERT INTO publicationSeul(id, titre, liensImages, datePublication, flux, utilisateur) VALUES (7, 'Plus de photos du Maroc', '["lienImage1", "lienImage2", "lienImage3"]','2018-02-15', 'Vacances', '123bella@gmail.com') ; -- 'Repair&fix'

INSERT INTO publicationPlusieurs(id, titre, description, datePublication, flux) VALUES (1, 'Update n°4 Projet', 'Mise à jour projet Superbowl', '2019-11-11', 'Superbowl') ; -- 'Marketingzone'
INSERT INTO publicationPlusieurs(id, titre, description, datePublication, flux) VALUES (2, 'Update n°5 Projet', 'Mise à jour projet Superbowl', '2019-11-12', 'Superbowl') ; -- 'Marketingzone'
INSERT INTO publicationPlusieurs(id, titre, description, datePublication, flux) VALUES (3, 'News 10/08/18', 'News 10/08/18', '2018-08-10', 'News') ; -- 'Tektonik'
INSERT INTO publicationPlusieurs(id, titre, description, datePublication, flux) VALUES (4, 'News 11/08/18', 'News 11/08/18', '2018-08-11', 'News') ; -- 'Tektonik'

INSERT INTO noteresponsable(id, titre, description, dateNoteResponsable, flux, responsable) VALUES (12, 'Note aux utilisateurs', 'Message','2018-03-01', 'Vacances', 'clementcollin@gmail.com') ; -- 'Repair&fix'
INSERT INTO noteresponsable(id, titre, description, dateNoteResponsable, flux, responsable) VALUES (13, 'Attention aux fraudes !!', 'Avertissement','2018-03-26', 'Vacances', 'clementcollin@gmail.com') ; -- 'Repair&fix'

INSERT INTO vote VALUES ('2019-11-06', 'like', 1, 'jeandujardin@yahoo.fr') ; -- 'Marketingzone'
INSERT INTO vote VALUES ('2019-11-06', 'like', 1, '123bella@gmail.com') ; -- 'Marketingzone'
INSERT INTO vote VALUES ('2019-11-07', 'dislike', 1, 'jacksparrrow@gmail.com') ; -- 'Marketingzone'
INSERT INTO vote VALUES ('2018-08-18', 'like', 5, 'dance4ever@gmail.com') ; -- 'Tektonik'
INSERT INTO vote VALUES ('2018-02-15', 'like', 6, 'clementcollin@gmail.com') ; -- 'Repair&fix'
INSERT INTO vote VALUES ('2018-02-16', 'dislike', 6, 'manu.dupre@yahoo.fr') ; -- 'Repair&fix'

INSERT INTO commentaire VALUES (1, '2019-11-06', 'ça ne me plait pas !', 1, 'jacksparrrow@gmail.com') ; -- 'Marketingzone'

INSERT INTO discussion VALUES ('jacksparrrow@gmail.com', 'dance4ever@gmail.com') ;
INSERT INTO discussion VALUES ('jacksparrrow@gmail.com', '123bella@gmail.com') ;
INSERT INTO discussion VALUES ('123bella@gmail.com', 'dance4ever@gmail.com') ;

INSERT INTO message VALUES (1, 'message', '2019-11-06', 'jacksparrrow@gmail.com', 'dance4ever@gmail.com') ;
INSERT INTO message VALUES (2, 'message', '2019-02-25', 'jacksparrrow@gmail.com', 'dance4ever@gmail.com') ;
INSERT INTO message VALUES (3, 'message', '2017-10-08', 'jacksparrrow@gmail.com', 'dance4ever@gmail.com') ;
INSERT INTO message VALUES (1, 'message', '2019-11-06', 'jacksparrrow@gmail.com', '123bella@gmail.com') ;
INSERT INTO message VALUES (1, 'message', '2015-02-20', '123bella@gmail.com', 'dance4ever@gmail.com') ;
INSERT INTO message VALUES (2, 'message', '2020-01-18', '123bella@gmail.com', 'dance4ever@gmail.com') ;

INSERT INTO reunion VALUES (1, '2019-12-13', '{"numero" : "12", "voie" : "Avenue de Flandres", "codePostal" : 33000}', 'Point du jour', 30) ;
INSERT INTO reunion VALUES (2, '2020-02-14', '{"numero" : "39", "voie" : "Rue fleurie", "codePostal" : 75000}', 'Urgent', 60) ;
INSERT INTO reunion VALUES (3, '2020-03-10', '{"numero" : "90", "voie" : "Rue claire", "codePostal" : 31000}', 'Strategie 2020', 120) ;
INSERT INTO reunion VALUES (4, '2020-03-10', '{"numero" : "90", "voie" : "Rue claire", "codePostal" : 31000}', 'Strategie 2020', 120) ;

INSERT INTO reunion_utilisateur VALUES (1, 'clementcollin@gmail.com') ;
INSERT INTO reunion_utilisateur VALUES (1, 'martin.come123@gmail.com') ;
INSERT INTO reunion_utilisateur VALUES (1, 'manu.dupre@yahoo.fr') ;
INSERT INTO reunion_utilisateur VALUES (2, '123bella@gmail.com') ;
INSERT INTO reunion_utilisateur VALUES (2, 'jacksparrrow@gmail.com') ;
INSERT INTO reunion_utilisateur VALUES (3, 'jeandujardin@yahoo.fr') ;
INSERT INTO reunion_utilisateur VALUES (3, '123bella@gmail.com') ;
INSERT INTO reunion_utilisateur VALUES (3, 'jennnnnn@gmail.com') ;
INSERT INTO reunion_utilisateur VALUES (3, 'jacksparrrow@gmail.com') ;

INSERT INTO publicationPlusieurs_utilisateur VALUES (1, 'jacksparrrow@gmail.com') ;
INSERT INTO publicationPlusieurs_utilisateur VALUES (1, 'jennnnnn@gmail.com') ;
INSERT INTO publicationPlusieurs_utilisateur VALUES (2, 'jacksparrrow@gmail.com') ;
INSERT INTO publicationPlusieurs_utilisateur VALUES (2, 'jennnnnn@gmail.com') ;
INSERT INTO publicationPlusieurs_utilisateur VALUES (3, 'dance4ever@gmail.com') ;
INSERT INTO publicationPlusieurs_utilisateur VALUES (3, 'leondanse@gmail.com') ;
INSERT INTO publicationPlusieurs_utilisateur VALUES (4, 'dance4ever@gmail.com') ;
INSERT INTO publicationPlusieurs_utilisateur VALUES (4, 'leondanse@gmail.com') ;
