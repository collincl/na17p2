DROP VIEW IF EXISTS v_nombre_groupes_redacteurs_flux_utilisateur ;
DROP VIEW IF EXISTS R5 ;
DROP VIEW IF EXISTS R4 ;
DROP VIEW IF EXISTS R3 ;
DROP VIEW IF EXISTS nombre_likes_publication ;
DROP VIEW IF EXISTS nombre_dislikes_publication ;
DROP VIEW IF EXISTS v_utilisateur ;
DROP VIEW IF EXISTS v_groupe ;
DROP VIEW IF EXISTS v_responsableflux ;
DROP VIEW IF EXISTS v_lecteur_flux ;
DROP VIEW IF EXISTS R1 ;
DROP VIEW IF EXISTS v_redacteur_flux ;
DROP VIEW IF EXISTS R2 ;
DROP VIEW IF EXISTS v_nombre_utilisateurs_reunion ;
DROP VIEW IF EXISTS v_nombre_utilisateurs_publicationPlusieurs ;
DROP VIEW IF EXISTS v_nombre_entreprises_groupes_utilisateur ;
DROP VIEW IF EXISTS v_flux_groupe_entreprise ;
DROP VIEW IF EXISTS v_entreprise_flux ;
DROP VIEW IF EXISTS v_entreprise_groupe ;
DROP VIEW IF EXISTS v_discussion_1_seul_utilisateur ;

DROP TABLE IF EXISTS reunion_utilisateur ;
DROP TABLE IF EXISTS publicationPlusieurs_utilisateur ;
DROP TABLE IF EXISTS groupe_flux ;
DROP TABLE IF EXISTS groupe_utilisateur ;
DROP TABLE IF EXISTS message ;
DROP TABLE IF EXISTS discussion ;
DROP TABLE IF EXISTS commentaire ;
DROP TABLE IF EXISTS vote ;
DROP TABLE IF EXISTS reunion ;
DROP TABLE IF EXISTS noteResponsable ;
DROP TABLE IF EXISTS publicationPlusieurs ;
DROP TABLE IF EXISTS publicationSeul ;
DROP TABLE IF EXISTS flux ;
DROP TABLE IF EXISTS groupe ;
DROP TABLE IF EXISTS responsableflux ;
DROP TABLE IF EXISTS utilisateur ;
DROP TABLE IF EXISTS entreprise ;
DROP TABLE IF EXISTS codePostal_ville_region ;

DROP TYPE IF EXISTS typegenre ;
DROP TYPE IF EXISTS typegroupe ;
DROP TYPE IF EXISTS typevote ;

        --------------------- creation types ---------------------------

CREATE TYPE typegenre AS ENUM ('homme', 'femme') ;
CREATE TYPE typegroupe AS ENUM ('lecteur', 'redacteur') ;
CREATE TYPE typevote AS ENUM ('like', 'dislike') ;

        --------------------- creation tables --------------------------

CREATE TABLE codePostal_ville_region (
  codePostal NUMERIC(5) PRIMARY KEY,
  ville VARCHAR(25) NOT NULL,
  region VARCHAR(25) NOT NULL
) ;

CREATE TABLE entreprise (
  siret NUMERIC(14) PRIMARY KEY,
  nom VARCHAR(25) NOT NULL,
  codePostal NUMERIC(5) NOT NULL,
  FOREIGN KEY (codePostal) REFERENCES codePostal_ville_region(codePostal),
  dateCreation DATE NOT NULL,
  CHECK (dateCreation < CURRENT_DATE)
) ;

CREATE TABLE utilisateur (
  email VARCHAR(40) PRIMARY KEY,
  nom VARCHAR(25) NOT NULL,
  prenom VARCHAR(25) NOT NULL,
  dateNaissance DATE NOT NULL,
  genre typegenre,
  pays VARCHAR(25) NOT NULL,
  metier VARCHAR(25) NOT NULL,
  entreprise NUMERIC(14) NOT NULL,
  FOREIGN KEY (entreprise) REFERENCES entreprise(siret),
  CHECK(email LIKE '%@%'),
  CHECK (dateNaissance < CURRENT_DATE)
) ;

CREATE TABLE responsableflux (
  email VARCHAR(40) PRIMARY KEY,
  FOREIGN KEY (email) REFERENCES utilisateur(email)
) ;

CREATE TABLE groupe (
  nom VARCHAR(25) PRIMARY KEY,
  type typegroupe NOT NULL,
  responsable VARCHAR(40) NOT NULL,
  FOREIGN KEY (responsable) REFERENCES utilisateur(email)
) ;

CREATE TABLE flux (
  nom VARCHAR(25) PRIMARY KEY,
  description VARCHAR(300),
  dateCreation DATE NOT NULL,
  responsable VARCHAR(40) NOT NULL,
  FOREIGN KEY (responsable) REFERENCES responsableFlux(email),
  CHECK (dateCreation < CURRENT_DATE)
) ;

CREATE TABLE publicationSeul (
  id INTEGER PRIMARY KEY,
  titre VARCHAR(25) NOT NULL,
  description VARCHAR(300),
  liensImages JSON,
  datePublication DATE,
  flux VARCHAR(25) NOT NULL,
  FOREIGN KEY (flux) REFERENCES flux(nom),
  utilisateur VARCHAR(40) NOT NULL,
  FOREIGN KEY (utilisateur) REFERENCES utilisateur(email)
) ;

CREATE TABLE publicationPlusieurs (
  id INTEGER PRIMARY KEY,
  titre VARCHAR(25) NOT NULL,
  description VARCHAR(300),
  liensImages JSON,
  datePublication DATE,
  flux VARCHAR(25) NOT NULL,
  FOREIGN KEY (flux) REFERENCES flux(nom)
) ;

CREATE TABLE noteresponsable (
  id INTEGER PRIMARY KEY,
  titre VARCHAR(25) NOT NULL,
  description VARCHAR(300),
  liensImagesImage JSON,
  dateNoteResponsable DATE,
  flux VARCHAR(25) NOT NULL,
  FOREIGN KEY (flux) REFERENCES flux(nom),
  responsable VARCHAR(40) NOT NULL,
  FOREIGN KEY (responsable) REFERENCES responsableFlux(email)
) ;

CREATE TABLE reunion (
  id INTEGER PRIMARY KEY,
  dateReunion DATE,
  lieuReunion JSON,
  titre VARCHAR(25) NOT NULL,
  duree INTEGER
  CHECK (dateReunion < CURRENT_DATE)
) ;

CREATE TABLE vote (
  dateVote DATE,
  vote typevote NOT NULL,
  publication INTEGER,
  FOREIGN KEY(publication) REFERENCES publicationSeul(id),
  utilisateur VARCHAR(40),
  FOREIGN KEY(utilisateur) REFERENCES utilisateur(email),
  PRIMARY KEY(publication, utilisateur)
) ;

CREATE TABLE commentaire (
  num INTEGER,
  dateCommentaire DATE,
  contenu VARCHAR(300),
  publication INTEGER,
  FOREIGN KEY(publication) REFERENCES publicationSeul(id),
  utilisateur VARCHAR(40) NOT NULL,
  FOREIGN KEY(utilisateur) REFERENCES utilisateur(email),
  PRIMARY KEY(num, publication)
) ;

CREATE TABLE discussion (
  utilisateur1 VARCHAR(40),
  FOREIGN KEY(utilisateur1) REFERENCES utilisateur(email),
  utilisateur2 VARCHAR(40),
  FOREIGN KEY(utilisateur2) REFERENCES utilisateur(email),
  PRIMARY KEY(utilisateur1, utilisateur2)
) ;

CREATE TABLE message (
  num INTEGER,
  contenu VARCHAR(300) NOT NULL,
  dateMessage DATE NOT NULL,
  discussion_utilisateur1 VARCHAR(40) NOT NULL,
  discussion_utilisateur2 VARCHAR(40) NOT NULL,
  FOREIGN KEY (discussion_utilisateur1, discussion_utilisateur2) REFERENCES discussion(utilisateur1, utilisateur2),
  PRIMARY KEY (num, discussion_utilisateur1, discussion_utilisateur2)
) ;

CREATE TABLE groupe_utilisateur (
  utilisateur VARCHAR(40),
  FOREIGN KEY(utilisateur) REFERENCES utilisateur(email),
  groupe VARCHAR(25),
  FOREIGN KEY(groupe) REFERENCES groupe(nom),
  PRIMARY KEY(utilisateur, groupe)
) ;

CREATE TABLE groupe_flux (
  groupe VARCHAR(25),
  FOREIGN KEY(groupe) REFERENCES groupe(nom),
  flux VARCHAR(25),
  FOREIGN KEY(flux) REFERENCES flux(nom),
  PRIMARY KEY(groupe, flux)
) ;

CREATE TABLE publicationPlusieurs_utilisateur (
  publicationPlusieurs INTEGER,
  FOREIGN KEY (publicationPlusieurs) REFERENCES publicationPlusieurs(id),
  utilisateur VARCHAR(40),
  FOREIGN KEY (utilisateur) REFERENCES utilisateur(email),
  PRIMARY KEY (publicationPlusieurs, utilisateur)
) ;

CREATE TABLE reunion_utilisateur (
  reunion INTEGER,
  FOREIGN KEY (reunion) REFERENCES reunion(id),
  utilisateur VARCHAR(40),
  FOREIGN KEY (utilisateur) REFERENCES utilisateur(email),
  PRIMARY KEY (reunion, utilisateur)
) ;
