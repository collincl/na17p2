## NOTE DE CLARIFICATION

__LISTE DES OBJETS__

\+ Les utilisateurs ont un email, un nom, un prénom, une date de naissance (i.e. on peut calculer leur âge), un genre, un métier et habitent dans un pays. Parmi les utilisateurs, il y a des responsables flux et des responsables groupe.

\+ Les entreprises ont un siret, un nom, une date de création et leur siège social se situe dans une ville, dans un pays.

\+ Les groupes ont un nom. Il y a deux types de groupes : les groupes lecteurs et les groupes rédacteurs.

\+ Les parutions ont un titre, une date de mise en ligne et peuvent contenir une image ou une description ou les deux. On distingue trois types de parutions : les publications seul, les publications à plusieurs et les notes responsable.

\+ Les flux de parutions ont un titre, une date de création et peuvent avoir une description.

\+ Les commentaires ont un contenu (texte) et une date.

\+ Les votes ont un contenu ("like" ou "dislike") et une date.

\+ Les messages ont un contenu (texte) et une date.

\+ Les discussions entre deux utilisateurs.

\+ Les réunions se déroulent dans un lieu, à une date et une heure précise. Elles ont une durée et un titre.


__LISTE DES ASSOCIATIONS__

\+ Les groupes sont des groupes d'une entreprise. Les entreprises peuvent avoir autant de groupes qu'elles le souhaitent.

\+ Les flux sont des groupes d'une entreprise. Les entreprises peuvent avoir autant de flux qu'elles le souhaitent.

\+ Les utilisateurs font partie de groupes (au moins un groupe). Les groupes peuvent contenir de nombreux utilisateurs.

\+ Chaque groupe est géré par un responsable groupe.

\+ Les groupes ont accès à des flux (un groupe ne peut avoir accès à un flux que s'il est de la même entreprise). Chaque flux possède au minimum un groupe lecteur et un groupe rédacteur.

\+ Chaque flux est géré par un responsable flux.

\+ Les flux contiennent des parutions. Une parution est contenue dans un seul flux.

\+ Chaque publication seul est mise en ligne par un utilisateur. Il n'y a pas de nombre maximum de publications par utilisateur.

\+ Chaque publication à plusieurs est mise en ligne par plusieurs utilisateurs. Tous les utilisateurs doivent appartenir à un groupe rédacteur du flux. Il n'y a pas de nombre maximum de publications à plusieurs par utilisateur.

\+ Chaque note responsable est mise en ligne par un responsable flux. Il n'y a pas de nombre maximum de notes responsable par responsable flux.

\+ Chaque commentaire appartient à une publication seul. Chaque publication peut être commentée de nombreuses fois.

\+ Chaque vote se rapporte à une publication seul. Chaque publication peut être like/dislike "un nombre illimité" de fois. <br>

Remarque : Etant produites par plusieurs utilisateurs nous supposons que les publications à plusieurs n'ont pas besoin être commentées ni like/dislike.

\+ Chaque commentaire est laissé par un utilisateur. Un utilisateur peut laisser de nombreux commentaires.

\+ Chaque vote est laissé par un utilisateur. Un utilisateur ne peut voter qu'une fois par publication seul.

\+ Deux utilisateurs peuvent avoir une discussion.

\+ Les discussions contiennent 1 ou plusieurs messages.

\+ Plusieurs utilisateurs prennent part à une réunion. Ils peuvent prendre part à plusieurs réunions à condition qu'elles n'aient pas lieu en même temps.



<!--
\+ Pour chaque _utilisateur_, une vue doit permettre d'afficher son "home" ou "profile".
-->
