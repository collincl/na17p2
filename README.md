### Clément COLLIN - PROJET 2 - NF17
#### Livrables
- Étape 1 à rendre dimanche 24 mai 23h59  : NDC, MCD
- Étape 2 à rendre dimanche 31 mai 23h59  : NDC, MCD, MLD, SQL CREATE et INSERT
- Étape 3 à rendre dimanche 7 juin 23h59  : NDC, MCD, MLD, SQL CREATE et INSERT, SQL SELECT, Complément : PostgreSQL/JSON (NA17, NF17)
- Étape 4 à rendre dimanche 14 juin 23h59 : Révision complète du projet (v2)

#### Sujet n°7 : Dashboard
https://librecours.net/dawa/projets-2020-II/co/dashboard.html

#### Objectif du projet
Réaliser un client web (frontend utilisateur), le backend et la structure de données associés permettant de montrer à nos investisseurs le fonctionnement de notre idée révolutionnaire. Une attention particulière sera apportée à construire un modèle de données robuste. En effet nos meilleurs experts marketing prévoient plusieurs millions d'utilisateurs dès la semaine de lancement.
